# Our Values

We only have two values:

1. Do it right; and
2. Do the right thing.

We believe these two simple guiding principles say more than 99% of the other wanky bullshit spouted
by modern companies.

## #1 of 2 - Do it right

![Value 1](/company/images/values-dir.jpg "Value #1 of 2")

Nothing at Billing Goat is done without thought and consideration. Cutting corners is the best way 
to fuck things up. 

We live and breathe, "Do it right". Before we even onboarded our first member, we were audit ready 
for ISO-27001:2022, SOC-2, SOX & GDPR! This is what doing it right looks like. We have policies,
and processes in place that allow us to move extremely quickly, with very little risk of missing
important details that can really fuck us up.

## #2 of 2 - Do the right thing

![Value 2](/company/images/values-dtrt.jpg "Value #2 of 2")

Arguably more important than doing things right, is doing the right thing. We believe everyone's 
mission in life should be to bring more good to the world than bad. Net positive impact.

Everything we do, we make sure it's the right thing. From our Employee Bill of Rights, to our baked
in commitments to make the world a better place - doing the right thing is in our DNA.

