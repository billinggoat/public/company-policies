# Culture

The culture at Billing Goat goes against most modern trends. We believe work is important for an 
individuals purpose, but not for their life overall. 

Our culture is guided by the following principles:

- We are not your family; and
- Your rights should be guaranteed.

Billing Goat does not try to create a "cult" mindset. We want everyone who works here to be 
individuals who define themselves by the awesome skills they possess and the things they achieve
in and outside of their work.

## We are not your family

Whilst we hope all employees of Billing Goat build long lasting friendships and professional 
partnerships, we are absolutely not your family. 

The fact that modern companies attempt to convince their employees they are part of a "family"
is so fucking toxic it defies explanation. We see this as the first step making you feel like
you owe, or are bound to a company emotionally.

Our job isn't to make you feel attached to Billing Goat. Our job is to give you an environment
where you can feel proud of what you do, get paid and share in the profits of your labour, and
hopefully, if you choose, give you all the tools and skills you need to build something for yourself.

Our measure of success is that you take our philosophy with you.

## Your rights should be guaranteed

Let's face it. You spend nearly as much time at work as you do in general society - so why do most 
workplaces not enshrine their employee's rights in contract?

Billing Goat has developed a very simple [Employee Bill of Rights](/company/culture/employee-bill-of-rights.md). It
is our commitment to our employees, and is written into every full-time employment contract.

# More culture @ Billing Goat

- [Fully Remote](/company/culture/fully-remote.md)
- [Be Private, Act Public](/company/culture/be-private-act-public.md)
- [Employee Bill of Rights](/company/culture/employee-bill-of-rights.md)

