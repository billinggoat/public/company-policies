# Fully Remote

Billing Goat is a fully remote company. We do not impose any geographical restrictions on our
employees as long as they are able to do what they are employed to do.

Remote work is guaranteed in the [Employee Bill of Rights](/company/culture/employee-bill-of-rights.md)
and part of every Billing Goat full-time employee contract.

## Why fully remote?

There is no doubt, offices are great. Offices allow people to come together, communicate with real
world queues, micro-expressions, hand gestures and all those little human things people either don't
think about, or take for granted.

But offices favour businesses, not the employee. Offices starve people of their time 
(travel), money (lunches) and life (seeing kids more than a couple of hours a day).

Do we believe Fully Remote is the best way to run a company? No. But if the founder isn't willing
to go into an office, why should anyone else?

## How we do it

In order for Fully Remote to work, good processes and policies need to be in place to ensure people
aren't wondering what to do... ever.

### Self-service

Everything at Billing Goat is self-service. Our employees use internal ITSM portals to request:
- Access to systems
- Access to equipment
- New software licenses
- Use of new packages in our product
- ... just about everything

Each request type has been carefully thought through (do it right) to ensure they are 100% compliant
with our policies, and that every employee is given the opportunity to objectively determine whether
their request meets our obligations; whether those be internal, regulatory or legal.

### Open information

Our internal wiki is completely open. This was one of the things that Atlassian got right, so we're
stealing the idea. Every bit of information should be available to our employees. We trust them
enough to build our product, care for our members and importantly, pay, so we should implicitly 
trust them with company information.

### Communication


